<p align="center"><img src="https://muzeum.pro/img/logo_muzeum-v-dark.svg"></p>

<h1 align="center">
    Muzeum Web Node
    <br/>
    基於區塊鏈的創意產業開放協定
</h1>

## Install

```sh
$ git clone https://github.com/KKFARM-inc/Muzeum-Web-Node.git
$ cd Muzeum-Web-Node
$ composer install
$ cp .env.example .env
$ php artisan key:generate
$ yarn
$ yarn run production
$ php artisan migrate

$ sudo cp muzeum-web-node.app /etc/nginx/sites-available/muzeum-web-node.app
$ sudo ln -s /etc/nginx/sites-available/muzeum-web-node.app /etc/nginx/sites-enable/muzeum-web-node.app
$ sudo service nginx restart
```