@extends('layouts.muzeum')

@section('title', config('app.name'))
@section('description', config('app.description'))

@section('content')
<div class="container">
    <form method="GET" action="{{ url('/') }}">
        <div class="form-row align-items-center">
            <div class="col-sm-3 my-3">
                <label class="sr-only" for="search_type">Type</label>
                <select id="search_type" name="search_type" class="form-control">
                    <option value="music_group" @if ($search_type=='music_group') selected @endif>Music Group</option>
                    <option value="music_album" @if ($search_type=='music_album') selected @endif>Muisc Album</option>
                    <option value="music_recording" @if ($search_type=='music_recording') selected @endif>Music Recording</option>
                    <option value="music_composition" @if ($search_type=='music_composition') selected @endif>Music Composition</option>
                </select>
            </div>
            <div class="col-sm-7 my-3">
                <label class="sr-only" for="search_input">Keywords</label>
                <input type="text" class="form-control" id="search_input" name="search_input" placeholder="Please input keywords" value="{{ $search_input }}">
            </div>
            <div class="col-auto my-3">
                <button type="submit" class="btn btn-primary">Search</button>
            </div>
        </div>
    </form>
    <div class="my-3">
        @foreach ($hits as $hit)
            @if ($search_type == 'music_group')
                @include('component.search_music_group')
            @elseif ($search_type == 'music_album')
                @include('component.search_music_album')
            @elseif ($search_type == 'music_recording')
                @include('component.search_music_recording')
            @elseif ($search_type == 'music_composition')
                @include('component.search_music_composition')
            @endif
        @endforeach
    </div>
</div>
@endsection
