<div class="row">
    <div class="col-sm-8 my-3">
        <h3>{{ $hit['_source']['name'] }}</h3>
        <dl class="dl-horizontal">
            <dt>DID</dt>
            <dd>{{ $hit['_source']['did'] }}</dd>
            <dt>IPFS Url</dt>
            <dd class="dont-break-out">
                <a href="{{ $hit['_source']['ipns_url'] }}" target="_blank">
                    {{ $hit['_source']['ipns_url'] }}
                </a>
            </dd>
            <dt>ISWC</dt>
            <dd>{{ $hit['_source']['iswcCode'] }}</dd>
            <dt>Album</dt>
            <dd>{{ $hit['_source']['inAlbum']['name'] }}</dd>
            <dt>Smart Contract</dt>
            <dd>
                {{ $hit['_source']['smart_contract_hash'] }}
            </dd>
            <dt>Composers</dt>
            <dd>
                <ul class="ml-3">
                @foreach ($hit['_source']['composer'] as $composer)
                    <li>
                        {{ $composer['name'] }} {{ $composer['ratio'] }}
                    </li>
                @endforeach
                </ul>
            </dd>
            <dt>Lyricists</dt>
            <dd>
                <ul class="ml-3">
                @foreach ($hit['_source']['lyricist'] as $lyricist)
                    <li>
                        {{ $lyricist['name'] }} {{ $lyricist['ratio'] }}
                    </li>
                @endforeach
                </ul>
            </dd>
        </dl>
    </div>
    <div class="col-sm-4 my-3">
        <h3>Lyric</h3>
        <p style="height: 300px; overflow: scroll; border: solid 1px #ccc; padding: 5px;">
            {!! nl2br($hit['_source']['lyrics']) !!}
        </p>
    </div>
</div>
<hr class="mb-6" />