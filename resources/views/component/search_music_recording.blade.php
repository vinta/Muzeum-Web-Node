<div class="row">
    <div class="col-sm-4 my-3">
        <img src="{{ $hit['_source']['thumbnailUrl'] }}" class="img-fluid rounded">
    </div>
    <div class="col-sm-8 my-3">
        <h3>{{ $hit['_source']['headline'] }}</h3>
        <dl class="dl-horizontal">
            <dt>DID</dt>
            <dd>{{ $hit['_source']['did'] }}</dd>
            <dt>IPFS Url</dt>
            <dd class="dont-break-out">
                <a href="{{ $hit['_source']['ipns_url'] }}" target="_blank">
                    {{ $hit['_source']['ipns_url'] }}
                </a>
            </dd>
            <dt>ISRC</dt>
            <dd>{{ $hit['_source']['isrcCode'] }}</dd>
            <dt>Album</dt>
            <dd>{{ $hit['_source']['inAlbum']['name'] }}</dd>
            <dt>Smart Contract</dt>
            <dd>
                {{ $hit['_source']['smart_contract_hash'] }}
            </dd>
            <dt>Artists</dt>
            <dd>
                <ul class="ml-3">
                @foreach ($hit['_source']['byArtist'] as $artist)
                    <li>
                        {{ $artist['name'] }}
                    </li>
                @endforeach
                </ul>
            </dd>
            <dt>Recording Share</dt>
            <dd>
                <ul class="ml-3">
                @foreach ($hit['_source']['recording_share'] as $recording_share)
                    <li>
                        {{ $recording_share['name'] }} {{ $recording_share['ratio'] }}
                    </li>
                @endforeach
                </ul>
            </dd>
        </dl>
    </div>
</div>
<hr class="mb-6" />