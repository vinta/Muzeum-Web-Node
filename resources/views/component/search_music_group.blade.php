<div class="row">
    <div class="col-sm-4 my-3">
        <img src="{{ $hit['_source']['image'] }}" class="img-fluid rounded">
    </div>
    <div class="col-sm-8 my-3">
        <h3>{{ $hit['_source']['alternateName'] }}</h3>
        <dl class="dl-horizontal">
            <dt>DID</dt>
            <dd>{{ $hit['_source']['did'] }}</dd>
            <dt>IPFS Url</dt>
            <dd class="dont-break-out">
                <a href="{{ $hit['_source']['ipns_url'] }}" target="_blank">
                    {{ $hit['_source']['ipns_url'] }}
                </a>
            </dd>
            <dt>Creative Works</dt>
            <dd>
                <ul class="ml-3">
                @foreach ($hit['_source']['album'] as $album)
                    <li>
                        {{ $album['name'] }}
                    </li>
                @endforeach
                </ul>
            </dd>
        </dl>
    </div>
</div>
<hr class="mb-6" />