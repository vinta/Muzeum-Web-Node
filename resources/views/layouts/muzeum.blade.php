<!DOCTYPE html>
<html class="sticky-ft-wrap">
@include('layouts.component.head')
<body class="sticky-ft-btm">
    <div id="app">
        @include('layouts.component.header')
        <div class="container my-2">
            @yield('content')
        </div>
        @include('layouts.component.footer')
    </div>
    @include('layouts.component.bottom_js')
    @yield('inject_script')
</body>
</html>