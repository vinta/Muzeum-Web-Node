<div class="sticky-ft">
    <footer class="container-fluid py-5">
        <div class="container">
            <img class="mb-4" src="/img/muzeum/logo_muzeum-h-monochrome-w.svg" alt="muzeum">
            <div class="row d-flex">
                <div class="col-sm-6">
                    <ul class="nav">
                        <li class="mr-sm-4 mr-2 active">
                            <a class="btn btn-sm btn-link px-0 text-light SHSTC-fw-medium" href="https://muzeum.pro/contact/">聯絡我們</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col mt-3 text-muted">
                    <small>Copyright © 2018 KKFarm Co., Ltd. All Rights Reserved.</small>
                </div>
            </div>
        </div>
    </footer>
</div>