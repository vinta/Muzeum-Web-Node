
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('example-component', require('./components/ExampleComponent.vue'));

const app = new Vue({
    el: '#app'
});

$(document).ready(function() {
    $('.jq-muzeum-intro').find('.navbar-brand').addClass('d-none');
    $('.jq-muzeum-intro').find('.navbar-toggler').addClass('d-none');
    $('.jq-muzeum-intro').find('.muzeum-nav').addClass('mx-auto');
    $(window).scroll(function(){
      if ($(window).scrollTop() > 100){
        $('.jq-muzeum-intro').find('.navbar').addClass('fixed-top bg-white-opacity-80 border-bottom-light').removeClass('my-2');
        $('.jq-muzeum-intro').find('.muzeum-nav').addClass('justify-content-end').removeClass('mx-auto');
        $('.jq-muzeum-intro').find('.navbar-brand').removeClass('d-none');
        $('.jq-muzeum-intro').find('.navbar-toggler').removeClass('d-none');
      }
      else {
        $('.jq-muzeum-intro').find('.muzeum-nav').removeClass('justify-content-end').addClass('mx-auto');
        $('.jq-muzeum-intro').find('.navbar-brand').addClass('d-none');
        $('.jq-muzeum-intro').find('.navbar-toggler').addClass('d-none');
        $('.jq-muzeum-intro').find('.navbar').addClass('my-2').removeClass('bg-white-opacity-80 border-bottom-light');
      }
    });
});