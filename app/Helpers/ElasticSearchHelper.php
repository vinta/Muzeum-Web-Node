<?php

namespace App\Helpers;

use Elasticsearch;

class ElasticSearchHelper
{
    /**
     * Search music group.
     *
     * @param string $key_word
     *
     * @return array
     */
    public static function searchMusicGroup($key_word)
    {
        $data = [
            'index' => 'muzeum_music_group',
            'type' => 'MusicGroup',
            'body' => [
                'query' => [
                    'bool' => [
                        'should' => [
                            [
                                'nested' => [
                                    'path' => 'track',
                                    'query' => [
                                        'multi_match' => [
                                            'query' => $key_word,
                                            'fields' => ['track.name']
                                        ]
                                    ]
                                ]
                            ],
                            [
                                'nested' => [
                                    'path' => 'album',
                                    'query' => [
                                        'multi_match' => [
                                            'query' => $key_word,
                                            'fields' => ['album.name']
                                        ]
                                    ]
                                ]
                            ],
                            [
                                'multi_match' => [
                                    'query' => $key_word,
                                    'fields' => ['name', 'alternateName', 'did']
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ];
        $return = Elasticsearch::search($data);
        return $return['hits']['hits'];
    }

    /**
     * Search music album.
     *
     * @param string $key_word
     *
     * @return array
     */
    public static function searchMusicAlbum($key_word)
    {
        $data = [
            'index' => 'muzeum_music_album',
            'type' => 'MusicAlbum',
            'body' => [
                'query' => [
                    'bool' => [
                        'should' => [
                            [
                                'nested' => [
                                    'path' => 'byArtist',
                                    'query' => [
                                        'multi_match' => [
                                            'query' => $key_word,
                                            'fields' => ['byArtist.name', 'byArtist.alternateName']
                                        ]
                                    ]
                                ]
                            ],
                            [
                                'multi_match' => [
                                    'query' => $key_word,
                                    'fields' => ['name', 'upcCode', 'did']
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ];
        $return = Elasticsearch::search($data);
        return $return['hits']['hits'];
    }

    /**
     * Search music recording.
     *
     * @param string $key_word
     *
     * @return array
     */
    public static function searchMusicRecording($key_word)
    {
        $data = [
            'index' => 'muzeum_music_recording',
            'type' => 'MusicRecording',
            'body' => [
                'query' => [
                    'bool' => [
                        'should' => [
                            [
                                'nested' => [
                                    'path' => 'byArtist',
                                    'query' => [
                                        'multi_match' => [
                                            'query' => $key_word,
                                            'fields' => ['byArtist.name', 'byArtist.alternateName']
                                        ]
                                    ]
                                ]
                            ],
                            [
                                'multi_match' => [
                                    'query' => $key_word,
                                    'fields' => ['name', 'isrcCode', 'did']
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ];
        $return = Elasticsearch::search($data);
        return $return['hits']['hits'];
    }

    /**
     * Search music composition.
     *
     * @param string $key_word
     *
     * @return array
     */
    public static function searchMusicComposition($key_word)
    {
        $data = [
            'index' => 'muzeum_music_composition',
            'type' => 'MusicComposition',
            'body' => [
                'query' => [
                    'bool' => [
                        'should' => [
                            [
                                'nested' => [
                                    'path' => 'composer',
                                    'query' => [
                                        'multi_match' => [
                                            'query' => $key_word,
                                            'fields' => ['composer.name']
                                        ]
                                    ]
                                ]
                            ],
                            [
                                'nested' => [
                                    'path' => 'lyricist',
                                    'query' => [
                                        'multi_match' => [
                                            'query' => $key_word,
                                            'fields' => ['lyricist.name']
                                        ]
                                    ]
                                ]
                            ],
                            [
                                'multi_match' => [
                                    'query' => $key_word,
                                    'fields' => ['name', 'iswcCode', 'lyrics', 'did']
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ];
        $return = Elasticsearch::search($data);
        return $return['hits']['hits'];
    }
}
