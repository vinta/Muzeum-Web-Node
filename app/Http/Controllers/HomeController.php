<?php

namespace App\Http\Controllers;

use App\Helpers\ElasticSearchHelper;

class HomeController extends Controller
{
    /**
     * Show the application index.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $search_type = request()->input('search_type');
        $search_input = request()->input('search_input');

        if (empty($search_type)) {
            $search_type = 'music_group';
        }

        $hits = array();
        if (! empty($search_input)) {
            switch ($search_type) {
            case 'music_group':
                $hits = ElasticSearchHelper::searchMusicGroup($search_input);
                break;
            case 'music_album':
                $hits = ElasticSearchHelper::searchMusicAlbum($search_input);
                break;
            case 'music_recording':
                $hits = ElasticSearchHelper::searchMusicRecording($search_input);
                break;
            case 'music_composition':
                $hits = ElasticSearchHelper::searchMusicComposition($search_input);
                break;
            }
        }

        return view('ipfs_search', [
            'search_type' => $search_type,
            'search_input' => $search_input,
            'hits' => $hits,
        ]);
    }

    /**
     * Show ipfs search
     *
     * @return \Illuminate\Http\Response
     */
    public function ipfsSearch()
    {
        $search_type = request()->input('search_type');
        $search_input = request()->input('search_input');

        if (empty($search_type)) {
            $search_type = 'music_group';
        }

        $hits = array();
        if (! empty($search_input)) {
            switch ($search_type) {
            case 'music_group':
                $hits = ElasticSearchHelper::searchMusicGroup($search_input);
                break;
            case 'music_album':
                $hits = ElasticSearchHelper::searchMusicAlbum($search_input);
                break;
            case 'music_recording':
                $hits = ElasticSearchHelper::searchMusicRecording($search_input);
                break;
            case 'music_composition':
                $hits = ElasticSearchHelper::searchMusicComposition($search_input);
                break;
            }
        }

        return view('ipfs_search', [
            'search_type' => $search_type,
            'search_input' => $search_input,
            'hits' => $hits,
        ]);
    }
}